function y=sizr(t,x,p)
% right-hand side for extended zombie infection model

p1=p.birthrate;     % Pi
p2=p.destruction;   % alpha
p3=p.infect;        % beta
p4=p.naturaldeath;  % delta
p5=p.resurrect;     % zeta
p6=p.transform;     % rho

y=[p1 - p3*x(1)*x(2) - p4*x(1); % susceptible (S)
    
    p6*x(4) + p5*x(3) - p2*x(1)*x(2); % zombie (Z)
    
    p4*x(1) + p2*x(1)*x(2) + p4*x(4) - p5*x(3); % removed/dead (R)
    
    p3*x(1)*x(2) - (p4+p6)*x(4)]; % infected (I)
end